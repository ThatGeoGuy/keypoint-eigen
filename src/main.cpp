// Keypoint-Eigen
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <cstdlib>
#include <getopt.h>
#include <iostream>
#include <list>
#include <memory>

#include "io/usage.h"
#include "io/read_xyz.h"
#include "keypoints/EigenKeypointDetector.h"

int main(int argc, char** argv)
{
    // Remove sync with c-style IO since I'm not using it anywhere.
    std::cout.sync_with_stdio(false);
    std::cerr.sync_with_stdio(false);
    std::cin.sync_with_stdio(false);

    // Ensure that output is fixed precision
    std::cout.setf(std::ios_base::fixed);
    std::cerr.setf(std::ios_base::fixed);

    auto output_filename = std::string{"-"};
    auto points_filename = std::string{"-"};

    int opt;
    while ((opt = getopt(argc, argv, "hi:o:")) != -1) {
        switch (opt) {
        case 'i':
            points_filename = std::string{optarg};
            break;
        case 'o':
            output_filename = std::string{optarg};
            break;
        case 'h':
            print_usage(std::cout, argv[0]);
            return EXIT_SUCCESS;
        default:
            print_usage(std::cerr, argv[0]);
            return EXIT_FAILURE;
        }
    }

    // Shared ptr because std::cin can't be uniquely owned.
    auto point_stream = std::shared_ptr<std::istream>{nullptr};
    if (points_filename == "-") {
        point_stream.reset(&std::cin, [](...){});
    } else {
        point_stream = std::make_shared<std::ifstream>(points_filename, std::ifstream::in);
    }

    const auto points = read_xyz(*point_stream);

    // Again, ptr is shared because std::cout can't be uniquely owned.
    auto output_stream = std::shared_ptr<std::ostream>{nullptr};
    if (output_filename == "-") {
        output_stream.reset(&std::cout, [](...){});
    } else {
        output_stream = std::make_shared<std::ofstream>(output_filename, std::ofstream::out);
    }

    auto keypoints = EigenKeypointDetector(500, 0.05, 0.2).keypoints(points);

    for (auto pt : keypoints) {
        (*output_stream) << pt << std::endl;
    }

    return EXIT_SUCCESS;
}
