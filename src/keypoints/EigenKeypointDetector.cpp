// Keypoint-Eigen
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "EigenKeypointDetector.h"

#include <algorithm>
#include <iterator>

#include "../math/covariance.h"
#include "../nearest_neighbour/NearestNeighbourTree.h"

struct EigenKeypointDetector::Impl {
    int max_k_neighbours;
    double neighbour_radius;
    double normalized_eigenvalue_threshold;

    Impl(int, double, double);

    Impl(Impl&) = delete;
    Impl& operator=(Impl&) = delete;

    Impl(Impl&&) = default;
    Impl& operator=(Impl&&) = default;

    ~Impl() = default;

    std::list<Point3D> keypoints(const std::list<Point3D>&);
};

EigenKeypointDetector::Impl::Impl(int k, double n, double t)
    : max_k_neighbours(std::move(k)),
      neighbour_radius(std::move(n)),
      normalized_eigenvalue_threshold(std::move(t))
{ }

std::list<Point3D> EigenKeypointDetector::Impl::keypoints(
    const std::list<Point3D>& points)
{
    using std::cbegin;
    using std::cend;
    using std::back_inserter;

    auto keypoints = std::list<Point3D>{};
    auto nntree = NearestNeighbourTree(points, 1);

    std::copy_if(begin(points), end(points), back_inserter(keypoints),
                 [&nntree, this](auto pt) {
                     auto neighbours = nntree.query_radius(
                         pt, this->neighbour_radius, this->max_k_neighbours);
                     auto cov = covariance(neighbours);
                     try {
                         auto evals = cov.eigenvalues().real();
                         evals = evals / evals.array().abs().maxCoeff();
                         return evals.array().abs().minCoeff() >=
                             this->normalized_eigenvalue_threshold;
                     } catch (std::exception& exn) {
                         return false;
                     }
                 });

    return keypoints;
}

EigenKeypointDetector::EigenKeypointDetector(
    int max_k_neighbours,
    double neighbour_radius,
    double eigen_threshold)
    : _pImpl(std::make_unique<EigenKeypointDetector::Impl>(
                 std::move(max_k_neighbours),
                 std::move(neighbour_radius),
                 std::move(eigen_threshold)))
{ }

EigenKeypointDetector::EigenKeypointDetector(EigenKeypointDetector&&) = default;
EigenKeypointDetector& EigenKeypointDetector::operator=(EigenKeypointDetector&&) = default;
EigenKeypointDetector::~EigenKeypointDetector() = default;

std::list<Point3D> EigenKeypointDetector::keypoints(
    const std::list<Point3D>& points)
{
    return _pImpl->keypoints(points);
}
