// Keypoint-Eigen
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <list>
#include <memory>
#include <vector>

#include "../points/Point3D.h"

class EigenKeypointDetector {
public:
    EigenKeypointDetector();
    EigenKeypointDetector(int, double, double);

    EigenKeypointDetector(EigenKeypointDetector&) = delete;
    EigenKeypointDetector& operator=(EigenKeypointDetector&) = delete;

    EigenKeypointDetector(EigenKeypointDetector&&);
    EigenKeypointDetector& operator=(EigenKeypointDetector&&);

    ~EigenKeypointDetector();

    /* Finds keypoints within the list of input points and returns a list
     * containing all the keypoints found.
     *
     * @param points - the list of input points to search for keypoints within.
     * @returns a list of identified keypoints denoting a subset of the input
     *          points.
     */
    std::list<Point3D> keypoints(const std::list<Point3D>&);

private:
    struct Impl;
    std::unique_ptr<Impl> _pImpl;
};
