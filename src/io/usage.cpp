// Keypoint-Eigen
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "usage.h"

void print_usage(std::ostream& sout, const std::string& program_name) noexcept
{
    using std::endl;

    sout << "Usage: " << program_name
         << " [-h] [-i POINTS_FILENAME] [-o OUTPUT_FILENAME]" << endl
         << "\t" << "-h\t" << "Print help text." << endl
         << "\t" << "-i\t" << "Specifies XYZ file to read 3D points from." << endl
         << "\t" << "-o\t" << "Gives name of XYZ file to write keypoints to." << endl;
}
