// Keypoint-Eigen
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <iostream>
#include <list>
#include <string>

#include "../exceptions/ArgumentException.h"
#include "../points/Point3D.h"

/* Reads an XYZ file into a collection of Point3D objects.
 * Throws an argument exception if the input stream is not good.
 *
 * @param sin - the input stream to read from.
 * @returns a collection of 3D point objects.
 */
std::list<Point3D> read_xyz(std::istream& sin);
