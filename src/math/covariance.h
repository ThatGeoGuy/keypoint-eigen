// Keypoint-Eigen
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <vector>

#include <Eigen/Dense>

#include "../points/Point3D.h"

/* Constructs a 3x3 covariance matrix from a collection of 3D points
 *
 * @param points - a vector containing 3D points
 * @returns a 3x3 covariance matrix.
 */
Eigen::MatrixXd covariance(const std::vector<Point3D>&);
