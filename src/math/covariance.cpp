// Keypoint-Eigen
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "covariance.h"

Eigen::MatrixXd covariance(const std::vector<Point3D>& points)
{
    auto data = Eigen::MatrixXd();
    data.resize(points.size(), 3);

    size_t i = 0;
    for (auto& pt : points) {
        data(i, 0) = pt.X;
        data(i, 1) = pt.Y;
        data(i, 2) = pt.Z;
        ++i;
    }

    auto centered = data.rowwise() - data.colwise().mean();
    return (centered.adjoint() * centered) / double(data.rows() - 1);
}
