// Keypoint-Eigen
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "NearestNeighbourTree.h"

#include <algorithm>
#include <iterator>
#include <ctgmath>

ANNpointArray point_list_to_ann_array(const std::list<Point3D>& points)
{
    auto pt_array = annAllocPts(points.size(), 3);

    size_t i = 0U;
    for (const auto pt : points) {
        pt_array[i][0] = pt.X;
        pt_array[i][1] = pt.Y;
        pt_array[i][2] = pt.Z;
        ++i;
    }
    return pt_array;
}

struct NearestNeighbourTree::Impl {
    ANNpointArray pt_array;
    std::unique_ptr<ANNkd_tree> tree;

    explicit Impl(const std::list<Point3D>&, int);

    Impl(Impl&) = delete;
    Impl& operator=(Impl&) = delete;

    Impl(Impl&&) = default;
    Impl& operator=(Impl&&) = default;

    // Regrettably can't be default because I need to dealloc ANN stuff
    ~Impl();

    std::vector<Point3D> query(const Point3D&, int) const;

    std::vector<Point3D> query_radius(const Point3D&, double, int) const;
};

NearestNeighbourTree::Impl::Impl(const std::list<Point3D>& points, int bucket_size)
    : pt_array(point_list_to_ann_array(points)),
      // Can't initialize tree until pt_array has properly been constructed.
      tree(nullptr)
{
    tree = std::make_unique<ANNkd_tree>(pt_array, points.size(), 3, bucket_size);
}

NearestNeighbourTree::Impl::~Impl()
{
    tree = nullptr;        // first dealloc the tree itself
    annDeallocPts(pt_array); // then dealloc the points themselves
}

std::vector<Point3D> NearestNeighbourTree::Impl::query(
    const Point3D& q, int k) const
{
    // The levels of gymnastics needed to use this library effectively are
    // batshit
    auto query_point = annAllocPt(3);
    query_point[0] = q.X;
    query_point[1] = q.Y;
    query_point[2] = q.Z;

    if (k > tree->nPoints()) {
        // FFS this fucking library uses ints for the output size of the index
        // array. Also, the manual says nothing about what happens if k >
        // nn_array size.
        // USE A SIZE_T AND SOME KIND OF STL TYPE REEEEEEEEEEEEEEE
        k = tree->nPoints();
    }

    ANNidxArray nn_indices = new ANNidx[k];
    tree->annkSearch(query_point, k, nn_indices, nullptr);

    auto neighbours = std::vector<Point3D>{};
    neighbours.reserve(k);

    for (auto i = 0; i < k; ++i) {
        neighbours.push_back(Point3D{
                pt_array[nn_indices[i]][0],
                pt_array[nn_indices[i]][1],
                pt_array[nn_indices[i]][2]});
    }
    delete [] nn_indices;
    return neighbours;
}

std::vector<Point3D> NearestNeighbourTree::Impl::query_radius(
    const Point3D& q,
    double radius,
    int max_k_points) const
{
    // The levels of gymnastics needed to use this library effectively are
    // batshit
    auto query_point = annAllocPt(3);
    query_point[0] = q.X;
    query_point[1] = q.Y;
    query_point[2] = q.Z;

    auto square_radius = pow(radius, 2);

    int k = 0;
    if (max_k_points) {
        k = max_k_points;
    } else {
        k = tree->annkFRSearch(query_point, square_radius, 0);
    }

    ANNidxArray nn_indices = new ANNidx[k];
    auto n = tree->annkFRSearch(query_point, square_radius, k, nn_indices);

    if (n < k) { k = n; }

    auto neighbours = std::vector<Point3D>{};
    neighbours.reserve(k);

    for (auto i = 0; i < k; ++i) {
        neighbours.push_back(Point3D{
                pt_array[nn_indices[i]][0],
                pt_array[nn_indices[i]][1],
                pt_array[nn_indices[i]][2]});
    }
    delete [] nn_indices;
    return neighbours;
}

NearestNeighbourTree::NearestNeighbourTree(
    const std::list<Point3D>& points,
    int bucket_size)
    : _pImpl(std::make_unique<NearestNeighbourTree::Impl>(points, std::move(bucket_size)))
{ }

NearestNeighbourTree::NearestNeighbourTree(NearestNeighbourTree&&) = default;
NearestNeighbourTree& NearestNeighbourTree::operator=(NearestNeighbourTree&&) = default;
NearestNeighbourTree::~NearestNeighbourTree() = default;

std::vector<Point3D> NearestNeighbourTree::query(const Point3D& q, int k) const
{
    return _pImpl->query(q, std::move(k));
}

std::vector<Point3D> NearestNeighbourTree::query_radius(
    const Point3D& q,
    double r,
    int k) const
{
    return _pImpl->query_radius(q, std::move(r), std::move(k));
}
