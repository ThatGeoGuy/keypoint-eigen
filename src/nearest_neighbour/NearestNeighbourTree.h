// Keypoint-Eigen
// Copyright (C) 2017 Jeremy Steward
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <list>
#include <memory>
#include <vector>

#include <ANN/ANN.h>

#include "../points/Point3D.h"

class NearestNeighbourTree {
public:
    explicit NearestNeighbourTree(const std::list<Point3D>&, int bucket_size);

    // This class will be move-only by default. I cannot think of too many use
    // cases where one would want to copy the nearest neighbour tree and not
    // change it significantly.
    NearestNeighbourTree(NearestNeighbourTree&) = delete;
    NearestNeighbourTree& operator=(NearestNeighbourTree&) = delete;

    // These will be default, but cannot be declared until after Impl is
    // defined
    NearestNeighbourTree(NearestNeighbourTree&&);
    NearestNeighbourTree& operator=(NearestNeighbourTree&&);
    ~NearestNeighbourTree();

    /* Queries the Nearest-Neighbour tree for the k nearest neighbours.
     *
     * @param point - the point to find the nearest neighbours of
     * @param k     - the maximum number of nearest neighbours to find. If the
     *                number of total points in the tree is less than k, then
     *                only that many points are returned.
     * @returns a vector containing the k nearest neighbours of the point.
     */
    std::vector<Point3D> query(const Point3D&, int) const;

    /* Queries the Nearest-Neighbour tree for up to k nearest neighbours
     * within radius r.
     *
     * @param point - the point to find the nearest neighbours of
     * @param r     - the fixed-radius cutoff to search for nearest neighbours.
     * @param k     - the maximum number of nearest neighbours to find. If the
     *                number of total points in the radius r is less than k,
     *                then only that many points are returned. Note if k = 0
     *                then all points in the radius are returned.
     * @returns a vector containing the k nearest neighbours of the point.
     */
    std::vector<Point3D> query_radius(const Point3D&, double, int) const;

private:
    struct Impl;
    std::unique_ptr<Impl> _pImpl;
};

