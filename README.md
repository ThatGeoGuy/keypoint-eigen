# Keypoint-Eigen

A small and poorly named C++ program to find keypoints in a small 3d point cloud.

## Build Instructions

The `keypoint-eigen` program is built using a standard Makefile. There is only
two real dependencies, one of which is on the
[Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page) library, the
other is [ANN](https://www.cs.umd.edu/~mount/ANN/). Pointing the Makefile to
the correct installation of Eigen3 is as simple as modifying the
`EIGEN3_INCLUDE_DIR` environment variable. ANN is a bit more difficult, as it
is necessary to set the right include and library path. Thus one can set
`ANN_INCLUDE_DIR` and `ANN_LIBRARY_DIR` for these, respectively. The tests use
[Catch](http://catch-lib.net), however, the corresponding header for Catch is
kept in the `tests/` directory, so there is no need to worry about which
version to download or use. Building is as simple as follows.

```
$ git clone https://bitbucket.org/ThatGeoGuy/keypoint-eigen.git
$ cd keypoint-eigen/
$ make -j 4 all RELEASE=1 # For release mode.
```

To run the test suite, you can make the tests, which will likewise call the test
runner:

```
$ make tests
```

Do note that you do not want to build the tests in parallel mode (i.e. with
arguments `-j 4`, it won't help), and you can modify the runner arguments (i.e.
which tests are run) at run-time by changing the `TESTARGS` environment
variable.

```
$ make tests TESTARGS=[similarity]
```

## Usage

Using the `keypoint-eigen` program is quite simple. To get started, you can run
`keypoint-eigen` with the help flag (`-h`):

```
$ keypoint-eigen
Usage: keypoint-eigen [-h] POINTS-FILE
        -h      Print help text.
```

Note that the one required argument, the points file, is a white-space delimited
text (XYZ) file with the following columns:

```
<X> <Y> <Z>
```

## LICENSE

Project is distributed under the terms of the GPLv3+. See `LICENSE` for more
details.
